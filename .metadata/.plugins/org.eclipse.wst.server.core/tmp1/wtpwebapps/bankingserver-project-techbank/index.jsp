<%@page import="java.util.ResourceBundle"%>
<html>

<%
ResourceBundle buildInfo = ResourceBundle.getBundle("build-info");
%>

<body>
<h1><b><%=buildInfo.getString("name")%></b></h1>

<ul>
<li>
	<b>Versi&oacute;n:</b>&nbsp;&nbsp; <%=buildInfo.getString("version")%>
</li>
<li>
	<b>Fecha de publicaci&oacute;n:</b> &nbsp;&nbsp;<%=buildInfo.getString("fecha.compilacion")%>&nbsp;Hs
</li>
<li style="margin-top: 30px;">
	<h3 style="color: green;">
		<b>Aplicaci&oacute;n desplegada correctamente.</b>
	</h3>
</li>
</ul>
</body>
</html>
