<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<div id="infoError" class="panel" selected="true">
		<fieldset style="font-size:16px; font-family:PFBeauSansProReg;">
			<div class="row sectionColor divError">
				<h1 id="pageTitle">Error</h1>
			</div>
			<div class="row divError" style="margin-bottom:20px;">
				<label class="labelError" style="color:grey; font-weight:normal;">
					 
					Ha ocurrido un problema por favor intentalo nuevamente.<br>
					 
				</label>
			</div>	
		</fieldset>
</div>

</body>
</html>