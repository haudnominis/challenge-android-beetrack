//Se determina si se trata de Safari
var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
var apiUrl = "";

(function($) {

var ajaxInvoker = $({});
	
$.ajaxInvoker = function (ajaxOpts) {
	ajaxOpts.url = apiUrl + ajaxOpts.url + "?access_token=" + getAccessToken();
	
	var promise;
	//Se setea el timeout para la peticion.
	if (ajaxInvokerTimeout != '-1') {
		ajaxOpts.timeout = ajaxInvokerTimeout;
	}
	//Si el cliente es safari:
	if (isSafari) {
		//Se matiene el modo de ejecucion original de la peticion.
		var isSync = false;
		/*Si es syncrono entonces agrego el shwoProgress y solicito confirmacion para cerrar la pagina*/
		if (!ajaxOpts.async) {
			ajaxOpts.beforeSend = function() { 
                                        $.showprogress();
                                        if (!ajaxOpts.escapeConfim) { 
                                            $(window).bind("beforeunload", function() { 
                                                                                return confirm("Seguro?"); 
                                                                            });
                                        }
                                    };
			isSync = true;
		}
		//Se setea async en true, para forzar que sea async
		ajaxOpts.async = true;
		//Si era Syncrono, entonces ejecuto con el hideProgress y se hace el unbind de la solicitud de confirmacion
		if (isSync) {
			promise = $.ajaxQueue(ajaxOpts).always(function() {
                                                $.hideprogress(); 
                                                $(window).unbind("beforeunload");
                                            }).fail(function(jqXHR, textStatus, errorThrown) {
                                            	failAjaxLog(ajaxOpts);
                                                displayTimeoutAjaxError(ajaxOpts);
                                            });
		} else {
			promise = $.ajaxQueue(ajaxOpts).fail(function(jqXHR, textStatus, errorThrown) {
                                                failAjaxLog(ajaxOpts);
                                                displayTimeoutAjaxError(ajaxOpts);
                                            });
		}
	} else {
		promise = $.ajax(ajaxOpts);
	}
	return promise;
};

})(jQuery);


/**
 Implementación de Plugin para JQuery para la implementación de una cola para que de invocaciones AJAX ASYNC s
 sean invocadas en una forma secuencial, simulando que se realizan en forma SYNC
**/

(function($) {

// Objeto vacio Query utilizado para la implementación de la Cola
var ajaxQueue = $({});


$.ajaxQueue = function( ajaxOpts ) {
    var jqXHR,
        dfd = $.Deferred(),
        promise = dfd.promise();
    
    //Se encola el requerimiento.
    
    ajaxQueue.queue( doRequest );

    //Se agrega un metodo abort
    promise.abort = function( statusText ) {

    	//Aborta el jqXHR si este esta activo
        if ( jqXHR ) {
            return jqXHR.abort( statusText );
        }

        var queue = ajaxQueue.queue(),
            index = $.inArray( doRequest, queue );

        if ( index > -1 ) {
            queue.splice( index, 1 );
        }

        dfd.rejectWith( ajaxOpts.context || ajaxOpts,
            [ promise, statusText, "" ] );

        return promise;
    };

    // Ejecuta el request actual.
    function doRequest( next ) {
        jqXHR = $.ajax( ajaxOpts )
            .done( dfd.resolve )
            .fail( dfd.reject )
            .then( next, next );
    }

    return promise;
};

})(jQuery);

function failAjaxLog(ajaxOpts) {
	
	/*var uuid = $('#uuid').val();
    $.ajax({type: 'POST',
                    url: apiUrl + 'ajaxTimeout',
                    headers: { 'Content-type': 'application/x-www-form-urlencoded; charset=UTF8'},
                    data: {'uuid':uuid, 'urlParam':ajaxOpts.url, 'dataParams': ajaxOpts.data},
                    async: true
                }).done(console.log('Timeout'));*/
	
	// En caso de error por timeout se redirije a la consolidada
	/*$(location).attr('href','../consolidada')*/
	//alert(ajaxOpts);
}

function displayTimeoutAjaxError(ajaxOpts) {

	/*Si es un widget, se redenriza el mensaje dentro del container del widget"*/
	if (ajaxOpts.url.includes("widget")) {
		var widgetId = getQueryVariable('widgetId', ajaxOpts.data);
		$('#' + widgetId).html(traducir('No se puede establecer conexion con el Servidor'))
		return;
	}

	if ($("#loginForm").length > 0) {
		showTimeoutError(traducir('No se puede establecer conexion con el Servidor'));
		cleanUuid();
	} else {
		showTimeoutError(traducir('No se puede establecer conexion con el Servidor'));
		if ($('#errorPanel').length > 0) {
			var errorpanel = $('#errorPanel').css({'margin-bottom': '400px'});
			$('#container').empty().append(errorpanel);
			
		}
	}
}

function getQueryVariable(variable, strData) {
    var query = strData;
    if (query.split != undefined) {
    	var vars = query.split('&');
    	for (var i = 0; i < vars.length; i++) {
    		var pair = vars[i].split('=');
    		if (decodeURIComponent(pair[0]) == variable) {
    			return decodeURIComponent(pair[1]);
    		}
    	}
    } else {
    	if ((Array.isArray != undefined) && (Array.isArray(query))) {
    		if (query[variable] != undefined) {
    			return query[variable];
    		}
    	}
    }
    console.log('Query variable %s not found', variable);
}

function setApiUrl (url){
	this.apiUrl=url;
}

function getAccessToken (){
	if(needsToRefreshToken()){
		getNewAccessToken(); 
	}
	return sessionStorage.getItem('access_token');
}

//** Funcion ajax que realiza el pedido de refresh_token (OAuth2) y devuelve el nuevo access_token ***
function getNewAccessToken(){
	var access_token;
	var refresh_token = sessionStorage.getItem('refresh_token');
	var url = apiUrl.replace("rest/viewIntegration/getViewContent/", "oauth/token");
	
	var data = {grant_type: "refresh_token", refresh_token: refresh_token};
	$.ajax({ url: url,
			data: data, 
			beforeSend: function (xhr) {
			    xhr.setRequestHeader ("Authorization", "Basic " + sessionStorage.getItem("client"));
			},
			async: false, 
			method: "POST",
			success: function(data,resp) {
				sessionStorage.setItem('access_token', data.access_token);
				sessionStorage.setItem('expires_in', data.expires_in);
				sessionStorage.setItem('refresh_token', data.refresh_token);
				sessionStorage.setItem('token_type', data.token_type);
				sessionStorage.setItem("date_token", Math.round( new Date().getTime() / 1000 ));				
			},
			error: function(data,resp) {
				sessionStorage.setItem('access_token', 'error');
			} 
	});
}

function needsToRefreshToken() {
   const horaActual = Math.round( new Date().getTime() / 1000 ) ;
   const horaActualRest = horaActual - sessionStorage.getItem('date_token');
   return (horaActualRest > sessionStorage.getItem('expires_in'));
}
