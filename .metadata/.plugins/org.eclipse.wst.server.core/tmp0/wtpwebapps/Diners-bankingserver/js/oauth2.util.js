// ** Funcion ajax que realiza el pedido de access_token (OAuth2) ***
function oAuth2AccessTokenRequest (url, clientId, clientSecret, user, pass, channel, targetChannel, callback){
	
	var data = {grant_type: "password", username: user, password: pass};
	$.ajax({ url: url + "oauth/token",
			data: data, 
			beforeSend: function (xhr) {
			    xhr.setRequestHeader ("Authorization", "Basic " + btoa(clientId + ":" + clientSecret));
				xhr.setRequestHeader('channel', channel);
				xhr.setRequestHeader('targetChannel', targetChannel);
			},
			async: true, 
			method: "POST",
			success: callback.success,
			error: callback.error
	});
}

// ** Funcion ajax que realiza el pedido de refresh_token (OAuth2) ***
function oAuth2RefreshTokenRequest (url, clientId, clientSecret, refresh_token, callback){
	
	var data = {grant_type: "refresh_token", refresh_token: refresh_token};
	$.ajax({ url: url + "oauth/token",
			data: data, 
			beforeSend: function (xhr) {
			    xhr.setRequestHeader ("Authorization", "Basic " + btoa(clientId + ":" + clientSecret));
			},
			async: true, 
			method: "POST",
			success: callback.success,
			error: callback.error
	});
}

// ** Funcion ajax que realiza el pedido de logout (OAuth2) ***
function oAuth2LogoutRequest (url, access_token, callback){
	
	$.ajax({ url: url + "rest/logout" + ((access_token!="")?("?access_token=" + access_token):""),
			data: "",
			async: true, 
			method: "POST",
			success: callback.success,
			error: callback.error
	});
}