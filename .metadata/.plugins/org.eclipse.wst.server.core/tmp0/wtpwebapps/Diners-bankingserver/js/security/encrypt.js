
function getKey(){
  return String(CryptoJS.lib.WordArray.random(16));
}

function encryptData(key, value){
  if(key && value){
    var simetricKey = CryptoJS.enc.Utf8.parse(key);
    // this is Base64-encoded encrypted data
    var encryptedData = CryptoJS.AES.encrypt(value, simetricKey, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    });
    return String(encryptedData);
  }
  return '';
}
