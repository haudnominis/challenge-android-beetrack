/**
 * Encrypt received text. with
 * @returns {string}
 */
function encryptText(publicKey, text){
  if (text && publicKey){
    var encrypt = new JSEncrypt();
    encrypt.setPublicKey(publicKey);
    return String(encrypt.encrypt(text));
  }
  return "";
}
