package com.beetrack.challenge.wallet.presentation.ui.transaction_history

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.beetrack.challenge.wallet.databinding.TransactionItemBinding
import com.beetrack.challenge.wallet.domain.entity.response.TransactionResponse

class TransactionHistoryAdapter(private val items: List<TransactionResponse>)
    : RecyclerView.Adapter<TransactionHistoryViewHolder>() {

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionHistoryViewHolder {
        val binding = TransactionItemBinding.inflate(LayoutInflater.from(parent.context))
        binding.root.layoutParams =
                RecyclerView.LayoutParams(
                        RecyclerView.LayoutParams.MATCH_PARENT,
                        RecyclerView.LayoutParams.WRAP_CONTENT
                )
        return TransactionHistoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TransactionHistoryViewHolder, position: Int) {
        holder.bind(items[position])
    }
}