package com.beetrack.challenge.wallet.data.network

import com.beetrack.challenge.wallet.domain.entity.response.AddressBalanceResponse
import com.beetrack.challenge.wallet.domain.entity.response.GenerateAddressResponse
import com.beetrack.challenge.wallet.domain.entity.response.WalletHistoryResponse
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface WalletService {
    @POST("addrs")
    suspend fun generateAddress(): GenerateAddressResponse

    @GET("addrs/{address}/balance")
    suspend fun getAddressBalance(@Path("address") address: String): AddressBalanceResponse

    @GET("addrs/{address}/full")
    suspend fun getWalletHistory(@Path("address") address: String): WalletHistoryResponse
}