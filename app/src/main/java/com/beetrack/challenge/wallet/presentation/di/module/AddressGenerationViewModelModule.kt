package com.beetrack.challenge.wallet.presentation.di.module

import androidx.lifecycle.ViewModel
import com.beetrack.challenge.wallet.presentation.ui.address_generation.AddressGenerationViewModel
import com.beetrack.challenge.wallet.presentation.ui.util.viewmodel.ViewModelKey
import com.beetrack.challenge.wallet.presentation.ui.util.viewmodel.ViewModelModuleUtils
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class AddressGenerationViewModelModule : ViewModelModuleUtils() {

    @Binds
    @IntoMap
    @ViewModelKey(AddressGenerationViewModel::class)
    abstract fun bindAddressGenerationViewModel(viewModel: AddressGenerationViewModel) : ViewModel

}