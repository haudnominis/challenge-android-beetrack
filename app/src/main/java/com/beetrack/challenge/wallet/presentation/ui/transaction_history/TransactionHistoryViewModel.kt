package com.beetrack.challenge.wallet.presentation.ui.transaction_history

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beetrack.challenge.wallet.domain.entity.response.WalletHistoryResponse
import com.beetrack.challenge.wallet.domain.interactor.GetAddressHistoryUseCase
import com.beetrack.challenge.wallet.presentation.ui.util.viewmodel.Failure
import com.beetrack.challenge.wallet.presentation.ui.util.viewmodel.Resource
import com.beetrack.challenge.wallet.presentation.ui.util.viewmodel.ResourceState
import com.beetrack.challenge.wallet.presentation.ui.util.viewmodel.post
import javax.inject.Inject

class TransactionHistoryViewModel @Inject constructor(
        private val getAddressHistoryUseCase: GetAddressHistoryUseCase
) : ViewModel() {

    var getAddressHistoryLiveData: MutableLiveData<Resource<WalletHistoryResponse>> = MutableLiveData()

    fun getAddressHistory(address: String) {
        getAddressHistoryLiveData.post(ResourceState.LOADING)

        fun handleSuccess(response: WalletHistoryResponse) = getAddressHistoryLiveData
                .post(ResourceState.SUCCESS, response)

        fun handleFailure(failure: Failure) = getAddressHistoryLiveData
                .post(ResourceState.ERROR, failure = failure)

        getAddressHistoryUseCase.invoke(address, ::handleSuccess, ::handleFailure)
    }
}