package com.beetrack.challenge.wallet.domain.entity.response

import com.google.gson.annotations.SerializedName

data class OutputResponse(
    val value: Double,
    val script: String,
    val addresses: List<String>,
    @SerializedName("script_type") val scriptType: String
)
