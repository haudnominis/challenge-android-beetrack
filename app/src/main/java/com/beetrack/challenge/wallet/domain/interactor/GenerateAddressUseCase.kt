package com.beetrack.challenge.wallet.domain.interactor

import com.beetrack.challenge.wallet.domain.IWalletRepository
import com.beetrack.challenge.wallet.domain.entity.response.GenerateAddressResponse
import javax.inject.Inject

class GenerateAddressUseCase @Inject constructor(private val iWalletRepository: IWalletRepository) : BaseUseCase<GenerateAddressResponse, Void?>() {
    override suspend fun run(params: Void?): GenerateAddressResponse = iWalletRepository.generateAddress()
}