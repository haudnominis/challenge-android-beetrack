package com.beetrack.challenge.wallet.data.reporistory

import com.beetrack.challenge.wallet.data.network.WalletService
import com.beetrack.challenge.wallet.domain.IWalletRepository
import com.beetrack.challenge.wallet.domain.entity.ApplicationParams
import com.beetrack.challenge.wallet.domain.entity.response.AddressBalanceResponse
import com.beetrack.challenge.wallet.domain.entity.response.GenerateAddressResponse
import com.beetrack.challenge.wallet.domain.entity.response.WalletHistoryResponse
import javax.inject.Inject

class WalletRepository @Inject constructor(
    private val walletService: WalletService,
    private val appParams: ApplicationParams
) : IWalletRepository {

    override suspend fun generateAddress(): GenerateAddressResponse {
        return walletService.generateAddress()
    }

    override suspend fun getAddressBalance(address: String): AddressBalanceResponse {
        return walletService.getAddressBalance(address)
    }

    override suspend fun getWalletHistory(address: String): WalletHistoryResponse {
        return walletService.getWalletHistory(address)
    }
}