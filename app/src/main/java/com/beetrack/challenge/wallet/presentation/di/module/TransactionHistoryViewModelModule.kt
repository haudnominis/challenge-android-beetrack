package com.beetrack.challenge.wallet.presentation.di.module

import androidx.lifecycle.ViewModel
import com.beetrack.challenge.wallet.presentation.ui.transaction_history.TransactionHistoryViewModel
import com.beetrack.challenge.wallet.presentation.ui.util.viewmodel.ViewModelKey
import com.beetrack.challenge.wallet.presentation.ui.util.viewmodel.ViewModelModuleUtils
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class TransactionHistoryViewModelModule : ViewModelModuleUtils() {

    @Binds
    @IntoMap
    @ViewModelKey(TransactionHistoryViewModel::class)
    abstract fun bindTransactionHistoryViewModel(viewModel: TransactionHistoryViewModel) : ViewModel

}