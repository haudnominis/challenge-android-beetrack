package com.beetrack.challenge.wallet.domain.entity

import android.os.Parcelable
import com.beetrack.challenge.wallet.connection.IBaseParams
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ApplicationParams(
    override val baseUrl: String?,
    override val isDebug: Boolean
) : IBaseParams, Parcelable
