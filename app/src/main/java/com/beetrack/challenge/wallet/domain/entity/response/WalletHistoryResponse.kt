package com.beetrack.challenge.wallet.domain.entity.response

import com.google.gson.annotations.SerializedName

data class WalletHistoryResponse(
    val address: String,
    val balance: Int,
    val txs: List<TransactionResponse>,
    @SerializedName("total_received") val totalReceived: Int,
    @SerializedName("total_sent") val totalSent: Int,
    @SerializedName("unconfirmed_balance") val unconfirmedBalance: Int,
    @SerializedName("final_balance")  val finalBalance: Int,
    @SerializedName("n_tx")  val nTx: Int,
    @SerializedName("unconfirmed_n_tx")  val unconfirmedNtx: Int,
    @SerializedName("final_n_tx")  val finalNtx: Int
)
