package com.beetrack.challenge.wallet.presentation.ui.application

import android.app.Application

abstract class BaseApplication : Application()