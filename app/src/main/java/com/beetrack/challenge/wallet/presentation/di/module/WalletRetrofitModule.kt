package com.beetrack.challenge.wallet.presentation.di.module

import com.beetrack.challenge.wallet.connection.BaseRetrofitModule
import com.beetrack.challenge.wallet.data.network.WalletService
import dagger.Module

@Module
class WalletRetrofitModule : BaseRetrofitModule<WalletService>(
        WalletService::class.java,
        40L,
        40L,
        40L
)