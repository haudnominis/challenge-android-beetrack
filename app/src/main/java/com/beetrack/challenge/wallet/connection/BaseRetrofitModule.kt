package com.beetrack.challenge.wallet.connection

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import java.io.IOException
import javax.inject.Singleton

@Module
abstract class BaseRetrofitModule<T>(
    private val service: Class<T>,
    private val connectTimeOut: Long = 120L,
    private val readTimeOut: Long = 120L,
    private val writeTimeOut: Long = 120L
) {
    @Provides
    @Singleton
    fun provideHttpClient(okHttpClient: OkHttpClient,
                            applicationParams: IBaseParams): T {
        return BaseRetrofit.Builder().apply {
            serviceUrl(applicationParams.baseUrl
                ?: throw IOException("you must add apiURL as a parameter"))
            isDebug(applicationParams.isDebug)
            okHttpClient(okHttpClient)
            connectTimeOut(connectTimeOut)
            writeTimeOut(writeTimeOut)
            readTimeOut(readTimeOut)
        }.build().create(service)
    }
}