package com.beetrack.challenge.wallet.presentation.ui.transaction_history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.beetrack.challenge.wallet.R
import com.beetrack.challenge.wallet.databinding.FragmentTransactionHistoryBinding
import com.beetrack.challenge.wallet.domain.entity.response.WalletHistoryResponse
import com.beetrack.challenge.wallet.presentation.ui.address_generation.AddressGenerationFragment
import com.beetrack.challenge.wallet.presentation.ui.application.appComponent
import com.beetrack.challenge.wallet.presentation.ui.util.BaseFragment
import com.beetrack.challenge.wallet.presentation.ui.util.viewmodel.ResourceState
import com.beetrack.challenge.wallet.presentation.ui.util.viewmodel.getViewModel
import com.beetrack.challenge.wallet.presentation.ui.util.Constant.KEY_ADDRESS
import com.beetrack.challenge.wallet.presentation.ui.util.Constant.KEY_ERROR_LOCATION
import com.beetrack.challenge.wallet.presentation.ui.util.Constant.KEY_ERROR_MESSAGE

class TransactionHistoryFragment : BaseFragment<FragmentTransactionHistoryBinding>() {
    private lateinit var viewModel: TransactionHistoryViewModel

    override fun injectDependencies() = appComponent().inject(this)

    override fun setBinding(inflater: LayoutInflater, container: ViewGroup?)
            : FragmentTransactionHistoryBinding = FragmentTransactionHistoryBinding
        .inflate(inflater, container, false)

    override fun initViewModel() {
        viewModel = getViewModel(viewModelFactory)

        viewModel.getAddressHistoryLiveData.observe(this@TransactionHistoryFragment, {
            it?.also {
                handleAddressHistory(it.status, it.data)
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.getString(KEY_ADDRESS)?.let {
            viewModel.getAddressHistory(it)
        } ?: kotlin.run {
            errorEvent(null)
        }

        binding.toolbar.title.text = getString(R.string.history_title)
        binding.toolbar.backButton.visibility = View.VISIBLE
        binding.toolbar.saveButton.visibility = View.GONE
        binding.toolbar.refreshBalance.visibility = View.GONE
        binding.toolbar.historyButton.visibility = View.GONE

        binding.toolbar.backButton.setOnClickListener {
            findNavController().navigate(R.id.action_transactionHistoryFragment_to_addressGenerationFragment)
        }
    }

    private fun handleAddressHistory(status: ResourceState, data: WalletHistoryResponse?) {
        when(status) {
            ResourceState.LOADING -> showIndicator()
            ResourceState.SUCCESS -> addressHistorySuccess(data)
            ResourceState.ERROR -> {
                hideIndicator()
                errorEvent(null)
            }
        }
    }

    private fun addressHistorySuccess(data: WalletHistoryResponse?) {
        hideIndicator()
        data?.let {
            val layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )
            val divider = DividerItemDecoration(
                binding.listTransaction.context,
                layoutManager.orientation
            )

            binding.listTransaction.layoutManager = layoutManager
            binding.listTransaction.addItemDecoration(divider)
            if (it.txs.isEmpty()) showEmptyList()
            else binding.listTransaction.adapter = TransactionHistoryAdapter(it.txs)
        }
    }

    private fun errorEvent(message: String?) {
        val bundle = Bundle().apply {
            putString(KEY_ERROR_LOCATION, TransactionHistoryFragment::class.simpleName)
            putString(KEY_ERROR_MESSAGE, message)
            putString(KEY_ADDRESS, arguments?.getString(KEY_ADDRESS))
        }
        findNavController()
            .navigate(R.id.action_transactionHistoryFragment_to_errorFragment, bundle)
    }

    private fun showEmptyList() {
        binding.emptyTransaction.emptyTransactionContainer.visibility = View.VISIBLE
        binding.listTransaction.visibility = View.GONE
    }

    private fun showIndicator() {
        binding.indicator.show()
        binding.listTransaction.visibility = View.GONE
    }

    private fun hideIndicator() {
        binding.indicator.hide()
        binding.listTransaction.visibility = View.VISIBLE
    }
}