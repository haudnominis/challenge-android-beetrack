package com.beetrack.challenge.wallet.presentation.ui.transaction_history

import androidx.recyclerview.widget.RecyclerView
import com.beetrack.challenge.wallet.databinding.TransactionItemBinding
import com.beetrack.challenge.wallet.domain.entity.response.TransactionResponse
import java.text.SimpleDateFormat
import java.util.*

class TransactionHistoryViewHolder(private val binding: TransactionItemBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(item: TransactionResponse) {
        val date = formatDate(item.received).split("-")
        binding.amount.text = String.format("%,.2f", item.total.toFloat())
        binding.day.text = date[0]
        binding.month.text = date[1]
    }

    private fun formatDate(time: String) : String {
        val checkFormat = time.split(".")

        val parser = if (checkFormat.size > 1) {
            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
        } else {
            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH)
        }
        val formatter = SimpleDateFormat("dd-MMM", Locale.ROOT)
        return parser.parse(time)?.let {
            formatter.format(it)
        } ?: kotlin.run { time }
    }
}