package com.beetrack.challenge.wallet.domain.interactor.error

import com.google.gson.annotations.SerializedName
import org.simpleframework.xml.Element

class HttpErrorResponse {
    @field:Element(name = "code", required = false)
    @field:SerializedName("code")
    var code: String? = null

    @field:Element(name = "message")
    @field:SerializedName("error_description")
    var message: String? = null

    @field:Element(name = "type")
    @field:SerializedName("error")
    var type: String? = null
}