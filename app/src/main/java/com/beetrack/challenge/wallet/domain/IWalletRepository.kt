package com.beetrack.challenge.wallet.domain

import com.beetrack.challenge.wallet.domain.entity.response.AddressBalanceResponse
import com.beetrack.challenge.wallet.domain.entity.response.GenerateAddressResponse
import com.beetrack.challenge.wallet.domain.entity.response.WalletHistoryResponse

interface IWalletRepository {
    @Throws(Exception::class)
    suspend fun generateAddress(): GenerateAddressResponse

    @Throws(Exception::class)
    suspend fun getAddressBalance(address: String): AddressBalanceResponse

    @Throws(Exception::class)
    suspend fun getWalletHistory(address: String): WalletHistoryResponse
}