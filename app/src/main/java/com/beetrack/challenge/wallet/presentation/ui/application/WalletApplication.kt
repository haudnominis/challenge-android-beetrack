package com.beetrack.challenge.wallet.presentation.ui.application

import android.app.Application
import com.beetrack.challenge.wallet.domain.entity.ApplicationParams
import com.beetrack.challenge.wallet.presentation.di.component.AppComponent
import com.beetrack.challenge.wallet.presentation.di.component.DaggerAppComponent
import com.beetrack.challenge.wallet.util.ApiConstant

class WalletApplication : Application() {
    companion object {
        internal var applicationComponent: AppComponent? = null
        lateinit var params: ApplicationParams
    }

    override fun onCreate() {
        super.onCreate()
        params = ApplicationParams(ApiConstant.baseUrl,true)
    }

    fun inject() {
        appComponent().inject(this)
    }
}

private fun buildDagger(): AppComponent {
    if (WalletApplication.applicationComponent == null) {
        WalletApplication.applicationComponent = DaggerAppComponent
            .builder()
            .appParams(WalletApplication.params)
            .build()
    }
    return WalletApplication.applicationComponent!!
}

internal fun appComponent() = buildDagger()