package com.beetrack.challenge.wallet.domain.interactor

import com.beetrack.challenge.wallet.domain.IWalletRepository
import com.beetrack.challenge.wallet.domain.entity.response.AddressBalanceResponse
import javax.inject.Inject

class GetAddressBalanceUseCase @Inject constructor(private val iWalletRepository: IWalletRepository) : BaseUseCase<AddressBalanceResponse, String?>() {
    override suspend fun run(params:String?): AddressBalanceResponse = iWalletRepository.getAddressBalance(params!!)
}