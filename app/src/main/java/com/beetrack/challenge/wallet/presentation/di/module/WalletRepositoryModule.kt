package com.beetrack.challenge.wallet.presentation.di.module

import com.beetrack.challenge.wallet.connection.IBaseParams
import com.beetrack.challenge.wallet.data.reporistory.WalletRepository
import com.beetrack.challenge.wallet.domain.IWalletRepository
import com.beetrack.challenge.wallet.domain.entity.ApplicationParams
import dagger.Binds
import dagger.Module

@Module
internal abstract class WalletRepositoryModule {

    @Binds
    abstract fun bindBaseParams(params: ApplicationParams): IBaseParams

    @Binds
    abstract fun bindWalletRepositoryModule(repository: WalletRepository): IWalletRepository
}