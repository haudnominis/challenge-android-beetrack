package com.beetrack.challenge.wallet.util

import android.app.Activity
import android.util.DisplayMetrics
import android.util.Log
import androidx.appcompat.widget.AppCompatImageView
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder

object QrUtil {
    fun displayQr(activity: Activity?, info: String?, view: AppCompatImageView) {
        val writer = MultiFormatWriter()
        val encoder = BarcodeEncoder()

        try {
            val displayMetrics = getMetrics(activity)
            val matrix = writer.encode(info, BarcodeFormat.QR_CODE,
                    displayMetrics.widthPixels, displayMetrics.widthPixels)
            view.setImageBitmap(encoder.createBitmap(matrix))
        } catch (e: WriterException) {
            Log.e("ASD", "Manejar error de escritura")
        }
    }

    private fun getMetrics(activity: Activity?) : DisplayMetrics {
        val displayMetrics = DisplayMetrics()
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.R) {
            activity?.display?.getRealMetrics(displayMetrics)
        } else {
            @Suppress("DEPRECATION")
            activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        }
        return displayMetrics
    }
}