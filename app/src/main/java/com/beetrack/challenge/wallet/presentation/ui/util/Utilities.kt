package com.beetrack.challenge.wallet.presentation.ui.util

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context

object Utilities {
    fun copyToClipboard(context: Context, text: String) : Boolean {
        val manager = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        manager.setPrimaryClip(ClipData.newPlainText("address", text))
        return true
    }
}