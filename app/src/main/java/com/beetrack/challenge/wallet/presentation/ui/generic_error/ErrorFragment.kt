package com.beetrack.challenge.wallet.presentation.ui.generic_error

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.beetrack.challenge.wallet.R
import com.beetrack.challenge.wallet.databinding.FragmentErrorBinding
import com.beetrack.challenge.wallet.presentation.ui.address_generation.AddressGenerationFragment
import com.beetrack.challenge.wallet.presentation.ui.application.appComponent
import com.beetrack.challenge.wallet.presentation.ui.transaction_history.TransactionHistoryFragment
import com.beetrack.challenge.wallet.presentation.ui.util.BaseFragment
import com.beetrack.challenge.wallet.presentation.ui.util.Constant.KEY_ADDRESS
import com.beetrack.challenge.wallet.presentation.ui.util.Constant.KEY_ERROR_LOCATION
import com.beetrack.challenge.wallet.presentation.ui.util.Constant.KEY_ERROR_MESSAGE

class ErrorFragment : BaseFragment<FragmentErrorBinding>() {
    private var errorLocation: String? = null
    private var errorMessage: String? = null
    private var address: String? = null

    override fun injectDependencies() = appComponent().inject(this)

    override fun setBinding(inflater: LayoutInflater, container: ViewGroup?)
    : FragmentErrorBinding = FragmentErrorBinding
        .inflate(inflater, container, false)

    override fun initViewModel() {/*Not implementation necessary*/}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.apply {
            errorLocation = getString(KEY_ERROR_LOCATION, null)
            errorMessage = getString(KEY_ERROR_MESSAGE, null)
            address = getString(KEY_ADDRESS, null)
        }

        binding.errorToolbar.title.text = getString(R.string.error_title)
        binding.errorToolbar.backButton.visibility = View.VISIBLE
        errorMessage?.let { binding.errorMessage.text = it }

        binding.errorToolbar.backButton.setOnClickListener { backEvent() }
    }

    private fun backEvent() {
        errorLocation?.let {
            when(it) {
                AddressGenerationFragment::class.simpleName -> {
                    findNavController()
                        .navigate(R.id.action_errorFragment_to_addressGenerationFragment)
                }
                TransactionHistoryFragment::class.simpleName -> {
                    val bundle = bundleOf(KEY_ADDRESS to address)
                    findNavController()
                        .navigate(R.id.action_errorFragment_to_transactionHistoryFragment, bundle)
                }
            }
        }
    }
}