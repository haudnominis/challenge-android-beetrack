package com.beetrack.challenge.wallet.presentation.di.component

import android.app.Application
import com.beetrack.challenge.wallet.domain.entity.ApplicationParams
import com.beetrack.challenge.wallet.presentation.ui.application.BaseApplication
import com.beetrack.challenge.wallet.connection.OkHttpModule
import com.beetrack.challenge.wallet.presentation.di.module.AddressGenerationViewModelModule
import com.beetrack.challenge.wallet.presentation.di.module.TransactionHistoryViewModelModule
import com.beetrack.challenge.wallet.presentation.di.module.WalletRepositoryModule
import com.beetrack.challenge.wallet.presentation.di.module.WalletRetrofitModule
import com.beetrack.challenge.wallet.presentation.ui.address_generation.AddressGenerationFragment
import com.beetrack.challenge.wallet.presentation.ui.generic_error.ErrorFragment
import com.beetrack.challenge.wallet.presentation.ui.transaction_history.TransactionHistoryFragment
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    WalletRepositoryModule::class,
    AddressGenerationViewModelModule::class,
    TransactionHistoryViewModelModule::class,
    OkHttpModule::class,
    WalletRetrofitModule::class])
interface AppComponent {
    @Component.Builder
    interface Builder {

        fun build(): AppComponent

        @BindsInstance
        fun appParams(params: ApplicationParams): Builder
    }

    fun inject(app: Application)
    fun inject(fragment: AddressGenerationFragment)
    fun inject(fragment: TransactionHistoryFragment)
    fun inject(fragment: ErrorFragment)
}