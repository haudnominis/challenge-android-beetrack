package com.beetrack.challenge.wallet.domain.interactor.error

data class BaseErrorResponse<T>(
    var status: Status? = null,
    var payload: T? = null
)

data class Status(
    var code: String? = null,
    var error: String? = null,
    var api: String? = null,
    var userMessage: String? = null,
    var techMessage: String? = null
)
