package com.beetrack.challenge.wallet.domain.entity.response

import com.google.gson.annotations.SerializedName

data class InputResponse(
    val script: String,
    val sequence: Double,
    val addresses: List<String>,
    val age: Double,
    @SerializedName("prev_hash") val prevHash: String,
    @SerializedName("output_index") val outputIndex: Int,
    @SerializedName("output_value") val outputValue: Double,
    @SerializedName("script_type") val scriptType: String
)
