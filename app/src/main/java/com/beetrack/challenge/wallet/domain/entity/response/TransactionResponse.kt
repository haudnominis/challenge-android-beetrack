package com.beetrack.challenge.wallet.domain.entity.response

import com.google.gson.annotations.SerializedName

data class TransactionResponse(
    val hash: String,
    val total: Double,
    val fees: Double,
    val size: Double,
    val confirmed: String,
    val received: String,
    val ver: Int,
    val preference: String,
    val confirmations: Int,
    val confidence: Double,
    val addresses: List<String>,
    val inputs: List<InputResponse>,
    val outputs: List<OutputResponse>,
    @SerializedName("block_hash") val blockHash: String,
    @SerializedName("block_height") val blockHeight: Int,
    @SerializedName("block_index") val block_index: Int,
    @SerializedName("vsize") val vSize: Double,
    @SerializedName("relayed_by") val relayedBy: String,
    @SerializedName("double_spend") val doubleSpend: Boolean,
    @SerializedName("vin_sz") val vinSz: Int,
    @SerializedName("vout_sz") val vouTsz: Int
)
