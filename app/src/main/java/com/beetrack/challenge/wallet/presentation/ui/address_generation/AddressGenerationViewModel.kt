package com.beetrack.challenge.wallet.presentation.ui.address_generation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beetrack.challenge.wallet.domain.entity.response.AddressBalanceResponse
import com.beetrack.challenge.wallet.domain.entity.response.GenerateAddressResponse
import com.beetrack.challenge.wallet.domain.interactor.GenerateAddressUseCase
import com.beetrack.challenge.wallet.domain.interactor.GetAddressBalanceUseCase
import com.beetrack.challenge.wallet.presentation.ui.util.viewmodel.Failure
import com.beetrack.challenge.wallet.presentation.ui.util.viewmodel.Resource
import com.beetrack.challenge.wallet.presentation.ui.util.viewmodel.ResourceState
import com.beetrack.challenge.wallet.presentation.ui.util.viewmodel.post
import javax.inject.Inject

class AddressGenerationViewModel @Inject constructor(
    private val generateAddressUseCase: GenerateAddressUseCase,
    private val getAddressBalanceUseCase: GetAddressBalanceUseCase
) : ViewModel() {
    var generateAddressLiveData: MutableLiveData<Resource<GenerateAddressResponse>> = MutableLiveData()
    var getAddressBalanceLiveData: MutableLiveData<Resource<AddressBalanceResponse>> = MutableLiveData()
    var address: String? = null

    fun generateAddress() {
        generateAddressLiveData.post(ResourceState.LOADING)

        fun handleSuccess(response: GenerateAddressResponse) = generateAddressLiveData
            .post(ResourceState.SUCCESS, response)

        fun handleFailure(failure: Failure) = generateAddressLiveData
            .post(ResourceState.ERROR, failure = failure)

        generateAddressUseCase.invoke(null, ::handleSuccess, ::handleFailure)
    }

    fun getAddressBalance() {
        getAddressBalanceLiveData.post(ResourceState.LOADING)

        fun handleSuccess(response: AddressBalanceResponse) = getAddressBalanceLiveData
                .post(ResourceState.SUCCESS, response)

        fun handleFailure(failure: Failure) = getAddressBalanceLiveData
                .post(ResourceState.ERROR, failure = failure)

        getAddressBalanceUseCase.invoke(address, ::handleSuccess, ::handleFailure)
    }
}