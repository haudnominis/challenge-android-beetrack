package com.beetrack.challenge.wallet.domain.interactor

import com.beetrack.challenge.wallet.domain.IWalletRepository
import com.beetrack.challenge.wallet.domain.entity.response.WalletHistoryResponse
import javax.inject.Inject

class GetAddressHistoryUseCase @Inject constructor(private val iWalletRepository: IWalletRepository) : BaseUseCase<WalletHistoryResponse, String?>() {
    override suspend fun run(params:String?): WalletHistoryResponse = iWalletRepository.getWalletHistory(params!!)
}