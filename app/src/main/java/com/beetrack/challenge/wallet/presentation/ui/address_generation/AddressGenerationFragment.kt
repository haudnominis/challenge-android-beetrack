package com.beetrack.challenge.wallet.presentation.ui.address_generation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.beetrack.challenge.wallet.R
import com.beetrack.challenge.wallet.databinding.FragmentAddressGenerationBinding
import com.beetrack.challenge.wallet.domain.entity.response.AddressBalanceResponse
import com.beetrack.challenge.wallet.domain.entity.response.GenerateAddressResponse
import com.beetrack.challenge.wallet.presentation.ui.application.appComponent
import com.beetrack.challenge.wallet.presentation.ui.util.BaseFragment
import com.beetrack.challenge.wallet.presentation.ui.util.Constant
import com.beetrack.challenge.wallet.presentation.ui.util.Utilities
import com.beetrack.challenge.wallet.presentation.ui.util.viewmodel.ResourceState
import com.beetrack.challenge.wallet.presentation.ui.util.viewmodel.getViewModel
import com.beetrack.challenge.wallet.util.QrUtil
import com.beetrack.challenge.wallet.presentation.ui.util.Constant.KEY_ADDRESS
import com.beetrack.challenge.wallet.presentation.ui.util.Constant.VALUE_EMPTY


class AddressGenerationFragment : BaseFragment<FragmentAddressGenerationBinding>() {
    private lateinit var viewModel: AddressGenerationViewModel

    override fun injectDependencies() = appComponent().inject(this)

    override fun setBinding(inflater: LayoutInflater, container: ViewGroup?)
            : FragmentAddressGenerationBinding = FragmentAddressGenerationBinding
        .inflate(inflater, container, false)

    override fun initViewModel() {
        viewModel = getViewModel(viewModelFactory)
        viewModel.generateAddressLiveData.observe(this@AddressGenerationFragment, {
            it?.also {
                handleGenerateAddress(it.status, it.data)
            }
        })

        viewModel.getAddressBalanceLiveData.observe(this@AddressGenerationFragment, {
            it?.also {
                handleAddressBalance(it.status, it.data)
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        displayView()
    }

    private fun handleGenerateAddress(status: ResourceState, data: GenerateAddressResponse?) {
        when(status) {
            ResourceState.LOADING -> showIndicator()
            ResourceState.SUCCESS -> generateAddressSuccess(data)
            ResourceState.ERROR -> {
                hideIndicator()
                errorEvent(null)
            }
        }
    }

    private fun handleAddressBalance(status: ResourceState, data: AddressBalanceResponse?) {
        when(status) {
            ResourceState.LOADING -> showIndicator()
            ResourceState.SUCCESS -> addressBalanceSuccess(data)
            ResourceState.ERROR -> {
                hideIndicator()
                errorEvent(null)
            }
        }
    }

    private fun generateAddressSuccess(data: GenerateAddressResponse?) {
        hideIndicator()
        data?.let {
            binding.balanceCard.visibility = View.GONE
            binding.toolbar.saveButton.visibility = View.VISIBLE
            viewModel.address = it.address
            binding.address.text = it.address
            QrUtil.displayQr(activity, it.address, binding.qrAddress)
        }
    }

    private fun addressBalanceSuccess(data: AddressBalanceResponse?) {
        hideIndicator()
        data?.let {
            walletBalanceDisplay()

            binding.balanceCard.visibility = View.VISIBLE
            binding.balance.text = String.format("%,.2f", it.balance.toFloat())
            binding.finalBalance.text = String.format("%,.2f", it.finalBalance.toFloat())
            binding.unconfirmedBalance.text = String.format("%,.2f", it.unconfirmedBalance.toFloat())
        }
    }

    private fun displayView() {
        showIndicator()
        viewModel.address = sharedPref?.getString(KEY_ADDRESS, VALUE_EMPTY)
        if (viewModel.address != null && viewModel.address != "") {
            binding.toolbar.title.text = getString(R.string.state_title)
            binding.address.text = viewModel.address
            binding.addAddress.visibility = View.GONE
            QrUtil.displayQr(activity, viewModel.address, binding.qrAddress)
            viewModel.getAddressBalance()
        } else {
            binding.toolbar.title.text = getString(R.string.generate_title)
            viewModel.generateAddress()
        }

        binding.address.setOnLongClickListener {
            showToast(requireContext(),
                getString(R.string.copy_to_clipboard),
                Toast.LENGTH_LONG)
            Utilities.copyToClipboard(requireContext(), binding.address.text.toString())
        }
        binding.addAddress.setOnClickListener { viewModel.generateAddress() }
        binding.toolbar.saveButton.setOnClickListener { saveEvent() }
        binding.toolbar.refreshBalance.setOnClickListener { viewModel.getAddressBalance() }
        binding.toolbar.historyButton.setOnClickListener { historyEvent() }
    }

    private fun walletBalanceDisplay() {
        binding.toolbar.title.text = getString(R.string.state_title)
        binding.toolbar.backButton.visibility = View.GONE
        binding.toolbar.saveButton.visibility = View.GONE
        binding.toolbar.refreshBalance.visibility = View.VISIBLE
        binding.toolbar.historyButton.visibility = View.VISIBLE
        binding.addAddress.visibility = View.GONE
    }

    private fun saveEvent() {
        sharedPref?.let {
            with(it.edit()) {
                putString(KEY_ADDRESS, viewModel.address)
                apply()
            }

            viewModel.getAddressBalance()
        } ?: kotlin.run {
            errorEvent(null)
        }
    }

    private fun historyEvent() {
        sharedPref?.let {
            val bundle = bundleOf(KEY_ADDRESS to it.getString(KEY_ADDRESS, VALUE_EMPTY))
            findNavController().navigate(R.id.action_addressGenerationFragment_to_transactionHistoryFragment, bundle)
        } ?: kotlin.run {
            errorEvent(null)
        }
    }

    private fun errorEvent(message: String?) {
        val bundle = Bundle().apply {
            putString(Constant.KEY_ERROR_LOCATION, AddressGenerationFragment::class.simpleName)
            putString(Constant.KEY_ERROR_MESSAGE, message)
        }
        findNavController()
            .navigate(R.id.action_addressGenerationFragment_to_errorFragment, bundle)
    }

    private fun showIndicator() {
        if (!binding.indicator.isShown) {
            binding.indicator.show()
            binding.scrollableContainer.visibility = View.GONE
        }
    }

    private fun hideIndicator() {
        binding.indicator.hide()
        binding.scrollableContainer.visibility = View.VISIBLE
    }
}