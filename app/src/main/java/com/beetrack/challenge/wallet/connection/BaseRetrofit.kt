package com.beetrack.challenge.wallet.connection

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class BaseRetrofit private constructor(
    private val baseUrl: String,
    private val isDebug: Boolean,
    private val connectTimeOut: Long,
    private val readTimeOut: Long,
    private val writeTimeOut: Long = 120L,
    private val converter: Gson,
    private val okHttpClient: OkHttpClient
) {

    fun getService(): Retrofit {
        val clientBuilder = okHttpClient.newBuilder()
        if(isDebug) {
            clientBuilder.addInterceptor(
                HttpLoggingInterceptor(HttpLoggingInterceptor.Logger.DEFAULT)
                    .apply { level =  HttpLoggingInterceptor.Level.BODY})
        }
        clientBuilder.connectTimeout(connectTimeOut, TimeUnit.SECONDS)
        clientBuilder.writeTimeout(readTimeOut, TimeUnit.SECONDS)
        clientBuilder.readTimeout(writeTimeOut, TimeUnit.SECONDS)
        val retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(clientBuilder.build())
            .addConverterFactory(GsonConverterFactory.create(converter))
        return retrofit.build()
    }

    class Builder {
        private var connectTimeOut: Long = 120L
        private var readTimeOut: Long = 120L
        private var writeTimeOut: Long = 120L
        private var serviceUrl: String? = null
        private var isDebug: Boolean = false
        private var okHttpClient: OkHttpClient = OkHttpClient.Builder().build()
        private var converter: Gson = GsonBuilder().setLenient().create()
        fun serviceUrl(url: String) = apply { this.serviceUrl = url }
        fun connectTimeOut(timeOut: Long) = apply { this.connectTimeOut = timeOut }
        fun readTimeOut(timeOut: Long) = apply { this.readTimeOut = timeOut }
        fun writeTimeOut(timeOut: Long) = apply { this.writeTimeOut = timeOut }
        fun isDebug(debug: Boolean) = apply { this.isDebug = debug }
        fun okHttpClient(okHttpClient: OkHttpClient) = apply { this.okHttpClient = okHttpClient }

        @Throws(IllegalArgumentException::class)
        fun build(): Retrofit {
            serviceUrl?.let {
                return BaseRetrofit(serviceUrl!!, isDebug, connectTimeOut, readTimeOut,
                    writeTimeOut, converter, okHttpClient).getService()
            } ?: kotlin.run {
                throw IllegalArgumentException("Connection URL cannot be null")
            }
        }

        @Throws(IllegalArgumentException::class)
        fun buildDefault(connectionUrl: String? = null): Retrofit {
            if (connectionUrl == null && serviceUrl == null)
                throw IllegalArgumentException("Connection URL cannot be null")
            return BaseRetrofit(serviceUrl?: connectionUrl!!, isDebug, connectTimeOut, readTimeOut,
                writeTimeOut, converter, okHttpClient).getService()
        }
    }
}