package com.beetrack.challenge.wallet.presentation.ui.util

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.beetrack.challenge.wallet.presentation.ui.util.viewmodel.ViewModelFactory
import javax.inject.Inject

abstract class BaseFragment<T : ViewBinding>  : Fragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    var sharedPref: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependencies()
        initViewModel()
        sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
    }

    protected lateinit var binding: T

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = setBinding(inflater, container)
        return binding.root
    }

    fun showToast(context: Context, text: String, lenght: Int) {
        Toast.makeText(context, text, lenght).show()
    }

    /**
     * Override this method in case you need to setBinding
     */

    abstract fun setBinding(inflater: LayoutInflater, container: ViewGroup?): T

    /**
    * Override this method in case you need to inject dependencies
    */
    protected abstract fun injectDependencies()

    /**
     * Override this method to initialize view model
     */
    protected abstract fun initViewModel()
}