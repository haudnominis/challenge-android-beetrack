package com.beetrack.challenge.wallet.domain.entity.response

data class GenerateAddressResponse(
    val private: String,
    val public: String,
    val address: String,
    val wif: String?
)
