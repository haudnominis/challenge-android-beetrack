package com.beetrack.challenge.wallet.connection

interface IBaseParams {
    val baseUrl: String?
    val isDebug: Boolean
}