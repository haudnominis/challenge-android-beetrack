package com.beetrack.challenge.wallet.domain.interactor

import com.beetrack.challenge.wallet.domain.interactor.error.BaseErrorResponse
import com.beetrack.challenge.wallet.domain.interactor.error.HttpErrorResponse
import com.beetrack.challenge.wallet.presentation.ui.util.viewmodel.Failure
import com.google.gson.Gson
import kotlinx.coroutines.*
import retrofit2.HttpException

abstract class BaseUseCase<Response, in Request> {
    private var job = Job()
    private var uiScope = CoroutineScope(Dispatchers.Main + job)

    @Throws(Exception::class)
    abstract suspend fun run(params: Request?): Response

    open fun invoke(params: Request?, onResult: (Response) -> Unit, onFailure: (Failure) -> Unit) {
        uiScope.launch {
            try {
                val result = withContext(Dispatchers.IO) { run(params) }
                onResult(result)
            } catch (e: Exception) {
                val response = (e as? HttpException)?.response()
                response?.errorBody()?.also {

                    try {
                        val errorBody = it.charStream()
                        val errorResponse = Gson().fromJson(
                                errorBody,
                                HttpErrorResponse::class.java
                        )

                        if (errorResponse.message == null) {
                            val errorBusiness = Gson().fromJson(
                                    errorBody,
                                    BaseErrorResponse::class.java
                            )
                            onFailure(errorBusiness.toFailure())
                        } else {
                            onFailure(errorResponse.toFailure())
                        }
                        return@launch

                    } catch (e: Exception) {
                        onFailure(Failure.ServerError)
                        return@launch
                    }
                } ?: onFailure(Failure.ServerError)
            }
        }
    }

    private fun HttpErrorResponse?.toFailure(): Failure {
        return if (this?.code == "401") {
            Failure.InvalidToken
        } else {
            Failure.Error(this?.message ?: "")
        }
    }

    private fun BaseErrorResponse<*>?.toFailure(): Failure {
        return Failure.BussinessError(this?.status?.code ?: "", this?.status?.techMessage ?: "")
    }

    open fun dispose() {
        job.cancel()
    }
}