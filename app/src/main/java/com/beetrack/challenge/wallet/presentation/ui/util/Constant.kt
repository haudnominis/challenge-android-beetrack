package com.beetrack.challenge.wallet.presentation.ui.util

object Constant {
    const val KEY_ADDRESS = "address"
    const val KEY_ERROR_LOCATION = "error_location"
    const val KEY_ERROR_MESSAGE = "error_message"
    const val VALUE_EMPTY = ""
}